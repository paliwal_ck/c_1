function AppConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
  'ngInject';
  $locationProvider.html5Mode(true);
  $httpProvider.interceptors.push('HttpInterceptor');

  $stateProvider.state('app', {
    abstract: true,
    templateUrl: 'layout/app-view.html'
  });
  $urlRouterProvider.otherwise('/');

}

export default AppConfig;
