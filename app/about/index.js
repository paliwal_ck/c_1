import angular from 'angular';
import AboutConfig from './about.config';
import AboutCtrl from './about.controller';

let aboutModule = angular.module('app.about', []);
aboutModule.config(AboutConfig);
aboutModule.controller('AboutCtrl', AboutCtrl);

export default aboutModule;
