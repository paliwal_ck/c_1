import angular from 'angular';

import HttpInterceptor from './HttpInterceptor.service';

let servicesModule = angular.module('app.services', []);

servicesModule.service('HttpInterceptor', HttpInterceptor);

export default servicesModule;
