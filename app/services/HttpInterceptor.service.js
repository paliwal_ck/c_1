class HttpInterceptor {
  constructor($q) {
    'ngInject';
    this.$q = $q;
  }

  // do whatever you want to do at services check by endpoint of api's
  // below request, requestError, response and responseError are available for modify req & response
  request(config) {
    return config;
  }

  requestError(config) {
    return $q.reject(config);
  }

  response(res) {
    return res;
  }

  responseError(res) {
    return $q.reject(res);
  }

}

export default HttpInterceptor;
